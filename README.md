## MiTrello

Aplicación creada con Laravel como cliente básico del sistema Trello. Se usó Bootstrap como framework de capa de presentación.

La aplicación se divide en capa de presentación encontrada en la carpeta resources/views/
- /autenticacion: Pantalla para autenticar al usuario.
- /consola: Pantallas de tableros generales y de un tablero específico.
- /plantillas: Plantilla que comparten algunas páginas.

Controladores web encontrados en la carpeta App/Http/Controllers

- ControladorConsola.php: Atiende todas las peticiones relacionadas a listar tableros y el CRUD sobre las tarjetas del usuario.
- ControladorLogin.php: Atiende las peticiones relacionadas al login de la aplicación

Apis a aplicaciones terceras en la carpeta App/Apis/Trello/

- ClienteTrello.php: Clase encargada de comunicarse con Trello, interpretar las respuestas y retornarlas en objetos del dominio

Entidades con las que se representa el dominio de la aplicación en la carpeta App/Entidades:

- Lista.php: Representa las listas de los tableros de Trello.
- Tablero.php: Representa los tableros de Trello.
- Tarjeta.php: Representa las tarjetas de los tableros de Trello.
- Usuario.php: Representa un usuario de Trello.

Recursos de capa de frontend ubicados en la carpeta public: CSS y JS.
	
