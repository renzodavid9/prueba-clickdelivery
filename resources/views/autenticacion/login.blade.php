<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MiTrello</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--Bootstrap-->
        <link rel="stylesheet" href="/css/app.css">
        <!-- Clases auxiliares -->
        <link rel="stylesheet" href="/css/general.css">
        <!-- Clases auxiliares responsive -->
        <link rel="stylesheet" href="/css/general-responsive.css">

        <!--JQuert-->
        <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <!--API Trello-->
        <script src="https://api.trello.com/1/client.js?key={{$trello_key}}"></script>

    </head>

    <body>
      <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                <div>
                  <h1 class="text-center">MiTrello</h1>
                  <p class="text-center">¡Bienvenido a MiTrello! Tu gestor personal de tableros. Por favor inicia sesión con tu usario de Trello para continuar</p>
                </div>

                <div class="gen-margin-top-5">
                  <button id="btn-login" type="submit" class="center-block btn btn-lg btn-primary">Ingresar con tu cuenta Trello</button>
                </div>

            </div>
          </div>
      </div>

      <!-- Bootstrap JavaScripts -->
      <script src="/js/app.js"></script>

      <!--js de la página-->
      <script src="/js/login/login.js"></script>
    </body>
</html>
