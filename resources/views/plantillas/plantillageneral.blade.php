<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>MiTrello</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--Bootstrap-->
        <link rel="stylesheet" href="/css/app.css">
        <!-- Clases auxiliares -->
        <link rel="stylesheet" href="/css/general.css">
        <!-- Clases auxiliares responsive -->
        <link rel="stylesheet" href="/css/general-responsive.css">

        @yield('estilos')

        <!--JQuery-->
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <!--API Trello-->
        <script src="https://api.trello.com/1/client.js"></script>
        <!--fontawesome-->
        <script src="https://use.fontawesome.com/744ac49598.js"></script>
        <!--Bootstrap-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>

    <body>

      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="/consola/inicio">Mitrello</a>
          </div>
          <ul class="nav navbar-nav">
            <li class="active"><a href="/consola/inicio">Inicio</a></li>
          </ul>


          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Salir</a></li>
            <li><a href="#"></a></li>
          </ul>

        </div>
      </nav>
      <div class="contenedor-principal">
        @yield('cuerpo')
      </div>

      <!-- Bootstrap JavaScripts -->
      <script src="/js/app.js"></script>

      <!--js de la página-->
      <script src="/js/login/login.js"></script>
    </body>
</html>
