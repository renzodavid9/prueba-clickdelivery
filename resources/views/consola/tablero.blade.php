@extends('plantillas.plantillageneral')

@section('estilos')
<link rel="stylesheet" href="/css/consola/tablero.css">
<script src="/js/tablero/tablero.js"></script>
@endsection

@section('cuerpo')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12">
      <h1>Tablero - {{$tablero->nombre}}</h1>
    </div>
  </div>

  @if ($tablero->tieneListas())
  <div class="row">
    <div class="col-xs-12">
      @foreach ($tablero->listas as $lista)
        <div class="row">
          <div class="col-xs-10">
            <div class="lista" id="lista-{{$lista->id}}">
              <div class="titulo-lista">
                <span>{{$lista->nombre}}</span>
              </div><div class="contenedor-tarjetas">
                <div class="ventana">
                @foreach($lista->tarjetas as $tarjeta)
                  <div class="tarjeta" id="tarjeta-{{$tarjeta->id}}">
                    <p id="text-{{$tarjeta->id}}" class="gen-margin-bottom-5">{{$tarjeta->nombre}}</p>
                    <button class="btn btn-primary" title="Editar" onclick="abrirModal('{{$tarjeta->id}}');"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger" title="Borrar" onclick="borrarTarjeta('{{$tarjeta->id}}')"><i class="fa fa-trash"></i></button>
                  </div>
                @endforeach
                </div>
              </div>

              <div class="contenedor-boton-lista">
                <button class="btn btn-primary" title="Crear nueva carta para {{$lista->nombre}}" onclick="abrirModalNuevaCarta('{{$lista->id}}')"><i class="fa fa-plus"></i></button>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  @else
  <div class="row">
    <div class="col-xs-12">
      <h1>No existen listas para este tablero</h1>
    </div>
  </div>
  @endif

</div>



<div class="modal fade" tabindex="-1" role="dialog" id="my-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title hidden editar" id="label-editar">Editar tarjeta</h4>
        <h4 class="modal-title hidden crear" id="label-crear">Crear tarjeta</h4>
      </div>
      <div class="modal-body">
        <textarea id="nuevo-texto" class="form-control"></textarea>
        <button class="btn btn-primary gen-margin-top-3 hidden editar" onclick="guardarCambio()" id="btn-guardar">Guardar</button>
        <button class="btn btn-primary gen-margin-top-3 hidden crear" onclick="crearTarjeta()" id="btn-crear">Crear</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection
