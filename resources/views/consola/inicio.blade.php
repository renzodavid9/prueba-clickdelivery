@extends('plantillas.plantillageneral')

@section('estilos')
<link rel="stylesheet" href="/css/consola/inicio.css">
@endsection

@section('cuerpo')
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h1>¡Bienvenido {{$usuario->nombre}}!</h1>
      <p class="h4">
        A continuación encontrarás la lista de tableros asociados a tu perfil
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <div id="titulo-tableros">
        <h2>Tableros</h2>
      </div>
    </div>
  </div>

  <div class="row">
    @if ($usuario->tieneTableros())
      @foreach ($usuario->listaTableros as $index=>$tablero)
        <div class="col-sm-6 col-lg-4">
          <a href="/consola/tablero/{{$tablero->id}}" class="link-tablero">
            <div class="tablero" data-idtablero="{{$tablero->id}}">
              {{$tablero->nombre}}
              <i class="fa fa-plus-square-o mas"></i>
            </div>
          </a>
        </div>
        <!--
        @if(($index+1) % 3 == 0)
        <div class="clearfix visible-lg"></div>
        <div class="col-lg-12 visible-lg">
          <div class="tablero-descripcion">
            Hola
            <a href="#" class="btn btn-success boton">Ir al tablero</a>
          </div>
        </div>
        @endif

        @if(($index+1) % 2 == 0)
        <div class="clearfix visible-sm visible-md"></div>
        <div class="col-sm-12 visible-sm visible-md">
          <div class="tablero-descripcion">

            <a href="#" class="btn btn-success boton">Ir al tablero</a>
          </div>
        </div>
        @endif

        <div class="col-sm-12 visible-xs">
          <div class="tablero-descripcion">

            <a href="#" class="btn btn-success boton">Ir al tablero</a>
          </div>
        </div>
      -->
      @endforeach
    @else

    @endif
  </div>
</div>
@endsection
