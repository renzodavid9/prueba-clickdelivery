$('#btn-login').click(function(){
  Trello.authorize({
    type: 'popup',
    name: 'MiTrello',
    scope: {
      read: 'true',
      write: 'true' },
    expiration: 'never',
    success: redirigirHome,
    error: mostrarError
  });
});


/*Función para redirigir al home del sistema con el usuario autenticado*/
function redirigirHome(){
  console.log(Trello.token());
  window.location.replace("/auth/"+Trello.token());
}

function mostrarError(){
  console.log("Algo salió mal");
}
