var tarjtetaId;
var listaId;
function abrirModal(id){
  tarjetaId = id;
  mostrarOcultar('.editar','.crear');
  var texto = $('#text-'+id).text();
  $('#nuevo-texto').val(texto);
  $('#my-modal').modal('show');
}

function guardarCambio(){
  $('#my-modal').modal('hide');
  var forma = new FormData();
  forma.append('id',tarjetaId);
  forma.append('nombre',$('#nuevo-texto').val());
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    url: '/consola/tarjeta',
    data: forma,
    success: cambiarTarjeta,
    error: error,
    cache: false,
    processData: false,
    contentType: false,
    context: this,
  });

}

function abrirModalNuevaCarta(idLista){
  listaId = idLista;
  mostrarOcultar('.crear','.editar');
  $('#nuevo-texto').val('');
  $('#my-modal').modal('show');
}

function borrarTarjeta(id){
  tarjetaId = id;
  if (confirm("¿Está seguro que desea ELIMINAR la tarjeta?")){
    llamarBorrado();
  }
}

function llamarBorrado(){
  var forma = new FormData();
  forma.append('id',tarjetaId);
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    url: '/consola/eliminartarjeta',
    data: forma,
    success: eliminarTarjeta,
    error: error,
    cache: false,
    processData: false,
    contentType: false,
    context: this,
  });
}

function eliminarTarjeta(){
  $('#tarjeta-'+tarjetaId).remove();
}

function cambiarTarjeta(respuesta){
  $('#text-'+respuesta.id).text(respuesta.nombre);
  alert("¡Tarjeta modificada con éxito!");
}


function error(){
  alert("Ha ocurrido un error, recargue la página");
}


function mostrarOcultar(elementoMostrar,elementoOcultar){
  $(elementoMostrar).removeClass('hidden');
  $(elementoOcultar).addClass('hidden');
}

function crearTarjeta(){
  var forma = new FormData();
  forma.append('id-lista',listaId);
  forma.append('nombre',$('#nuevo-texto').val());
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    url: '/consola/creartarjeta',
    data: forma,
    success: anadirTarjeta,
    error: error,
    cache: false,
    processData: false,
    contentType: false,
    context: this,
  });
}

function anadirTarjeta(respuesta){
  location.reload();
}
