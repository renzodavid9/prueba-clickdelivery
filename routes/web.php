<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Middleware para verificar si no estoy logueado en trello*/
Route::group(['middleware' => 'trellonologin'], function () {
  /*Ruta home*/
  Route::get('/', 'ControladorLogin@get');
  /*Ruta para autenticar al usuario de trello en el sistema MiTrello*/
  Route::get('auth/{token}', 'ControladorLogin@autenticar');
});



/*Grupo de rutas para manejar Trello desde MiTrello*/
/*Middleware para verificar si estoy logueado en trello*/
Route::group(['middleware' => 'trellologin'], function () {

    Route::group(['prefix' => 'consola'], function () {

      Route::get('inicio','ControladorConsola@get');
      Route::get('tablero/{idtablero}','ControladorConsola@gettablero');
      Route::post('tarjeta','ControladorConsola@actualizartarjeta');
      Route::post('creartarjeta','ControladorConsola@creartarjeta');
      Route::post('eliminartarjeta','ControladorConsola@eliminartarjeta');
      
    });

});
