<?php

namespace App\Http\Middleware;

use Closure;

class VerificarNoLoginTrello
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ($request->session()->has('trello_usuario_key')){
        return redirect('/consola/inicio');
      }
      return $next($request);
    }
}
