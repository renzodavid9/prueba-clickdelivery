<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apis\Trello\ClienteTrello;
use App\Entidades\Usuario;
use Log;

class ControladorConsola extends Controller
{
    /*Funcion para obtener la pantalla de inicio una vez logueado*/
    public function get(Request $request){
      $trello_usuario_key = $request->session()->get('trello_usuario_key');

      $usuario = $this->consultarTableros($trello_usuario_key);

      if ($usuario !== NULL){
        return view('consola.inicio',[
          'trello_key' => config('app.trello_key'),
          'usuario' => $usuario,
        ]);
      }
      return 'ERROR --- Intentelo nuevamente';
    }
    private function consultarTableros($trello_usuario_key){
      $usuario = ClienteTrello::obtenerTablerosUsuario($trello_usuario_key,config('app.trello_key'));
      return $usuario;
    }

    /*Funcion para obtener la pantalla de un tablero especifico*/
    public function gettablero(Request $request, $idtablero){
      $trello_usuario_key = $request->session()->get('trello_usuario_key');
      $tablero = $this->consultarTablero($trello_usuario_key,$idtablero);
      if ($tablero !== NULL){
        return view ('consola.tablero',[
          'trello_key' => config('app.trello_key'),
          'tablero' => $tablero,
        ]);
      }
      return 'ERROR --- Intentelo nuevamente';
    }

    private function consultarTablero($trello_usuario_key,$idtablero){
      return ClienteTrello::obtenerTablero($trello_usuario_key,config('app.trello_key'),$idtablero);
    }

    public function actualizartarjeta(Request $request){
      $body = $request->all();
      $trello_usuario_key = $request->session()->get('trello_usuario_key');
      $respuesta = ClienteTrello::actualizarTarjeta($trello_usuario_key,config('app.trello_key'),$body['id'],$body['nombre']);
      if ($respuesta != null){
        return response()->json([
          'nombre' => $respuesta->nombre,
          'id' => $respuesta->id,
        ]);
      }
      return response('',500);
    }

    public function eliminartarjeta(Request $request){
      Log::error('----------------------------------------------------------------------------------');
      $body = $request->all();
      $trello_usuario_key = $request->session()->get('trello_usuario_key');
      $respuesta = ClienteTrello::eliminarTarjeta($trello_usuario_key,config('app.trello_key'),$body['id']);
      if ($respuesta){
        return response('',200);
      }
      return response('',500);
    }

    public function creartarjeta(Request $request){
      $body = $request->all();
      $trello_usuario_key = $request->session()->get('trello_usuario_key');
      $respuesta = ClienteTrello::crearTarjeta($trello_usuario_key,config('app.trello_key'),$body['id-lista'],$body['nombre']);
      if ($respuesta != null){
        return response()->json([
          'nombre' => $respuesta->nombre,
          'id' => $respuesta->id,
        ]);
      }
      return response('',500);
    }
}
