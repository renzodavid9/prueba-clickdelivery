<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControladorLogin extends Controller
{
    /*Funcion para obtener la pantalla de login*/
    public function get(){
      return view('autenticacion.login',[
        'trello_key' => config('app.trello_key')
      ]);
    }

    /*Funciona para autenticar al usuario guardando su trello token en la sesion*/
    public function autenticar(Request $request,$token){
      $request->session()->put('trello_usuario_key',$token);
      return redirect('consola/inicio');
    }
}
