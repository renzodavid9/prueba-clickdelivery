<?php

namespace App\Entidades;

class Lista
{
  private $id;
  private $nombre;
  private $tarjetas;

  function __construct($id,$nombre) {
    $this->id = $id;
    $this->nombre = $nombre;
    $this->tarjetas = array();
  }
  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function anadirTarjeta($tarjeta){
    $this->tarjetas[] = $tarjeta;
  }

  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }
    return $this;
  }
}
