<?php

namespace App\Entidades;

class Tarjeta
{
  private $id;
  private $nombre;

  function __construct($id,$nombre) {
    $this->id = $id;
    $this->nombre = $nombre;
  }
  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }
    return $this;
  }
}
