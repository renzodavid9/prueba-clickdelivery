<?php

namespace App\Entidades;

class Tablero
{
  private $id;
  private $nombre;
  private $listas;

  function __construct($id,$nombre) {
    $this->id = $id;
    $this->nombre = $nombre;
    $this->listas = array();
  }

  public function anadirLista($lista){
    $this->listas[] = $lista;
  }

  public function tieneListas(){
    return sizeof($this->listas) > 0? true : false;
  }

  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }
    return $this;
  }
}
