<?php

namespace App\Entidades;

class Usuario
{
    private $nombre;
    private $listaTableros;

    function __construct($nombre) {
      $this->nombre = $nombre;
      $this->listaTableros = array();
    }

    public function anadirTablero($tablero){
      $this->listaTableros[] = $tablero;
    }

    public function tieneTableros(){
      return sizeof($this->listaTableros) > 0? true : false;
    }

    public function __get($property) {
      if (property_exists($this, $property)) {
        return $this->$property;
      }
    }

    public function __set($property, $value) {
      if (property_exists($this, $property)) {
        $this->$property = $value;
      }
      return $this;
    }

}
