<?php

namespace App\Apis\Trello;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Entidades\Usuario;
use App\Entidades\Tablero;
use App\Entidades\Lista;
use App\Entidades\Tarjeta;
use Log;

class ClienteTrello
{

  /* Constantes con las rutas del API REST de Trello*/

  /*Consulta de tableros*/
  const URL_LISTAR_TABLEROS = 'https://api.trello.com/1/members/me?fields=username,fullName,url&boards=all&board_fields=name&organizations=all&organization_fields=displayName';

  /*Consulta de listas de un tablero*/
  const URL_OBTENER_TABLERO_1 = 'https://api.trello.com/1/boards/';
  const URL_OBTENER_TABLERO_2 = '?fields=id,name,idOrganization,dateLastActivity&lists=open&list_fields=id,name';

  /*Cartas de un tablero*/
  const URL_OBTENER_CARTAS = 'https://api.trello.com/1/list/';
  const URL_OBTENER_CARTAS2 = '?fields=name&cards=open&card_fields=name';

  /*Editar cartas de un tablero*/
  const URL_EDITAR_CARTA = 'https://api.trello.com/1/cards/';
  const URL_EDITAR_CARTA_2 = '?name=';

  /*Crear nueva carta*/
  const URL_CREAR_CARTA = 'https://api.trello.com/1/lists/';
  const URL_CREAR_CARTA_2 = '/cards?name=';

  /*Eliminar nueva tarjeta*/
  const URL_ELIMINAR_CARTA = 'https://api.trello.com/1/cards/';

  /*Método para consultar los tableros del usuario
  *
  *   $token_usuario: token de trello para hacer las peticiones
  */
  public static function obtenerTablerosUsuario($token_usuario,$app_key) {
    $client = new Client();
    $resultado = $client->get(ClienteTrello::URL_LISTAR_TABLEROS.'&key='.$app_key.'&token='.$token_usuario);
    if ($resultado->getStatusCode() == 200){
      return ClienteTrello::armarRespuestaTableros($resultado->getBody());
    }
    return null;
  }

  /*Funcion para pasar de objeto json a entidades del sistema
  *
  *   $respuestaJSON: objeto JSON
  */
  private static function armarRespuestaTableros($respuestaJSON){
    $tableros = json_decode($respuestaJSON,true);
    $usuario = new Usuario($tableros['fullName']);
    foreach ($tableros['boards'] as $tablero){
      $tablero = new Tablero($tablero['id'],$tablero['name']);
      $usuario->anadirTablero($tablero);
    }
    return $usuario;
  }

  /*Función para obtener un tablero completo
   *
   *Se envía por parametro el token del usuario, la llave de la app y el id del tablero a cagar
   **/
  public static function obtenerTablero($token_usuario,$app_key,$idtablero){
    $client = new Client();
    $resultado = $client->get(ClienteTrello::URL_OBTENER_TABLERO_1.$idtablero.ClienteTrello::URL_OBTENER_TABLERO_2.'&key='.$app_key.'&token='.$token_usuario);
    if ($resultado->getStatusCode() == 200){
      return ClienteTrello::armarTablero($resultado->getBody(),$token_usuario,$app_key,$idtablero);
    }
    return null;
  }

  private static function armarTablero($respuestaJSON,$token_usuario,$app_key,$idtablero){
    $resp_tablero = json_decode($respuestaJSON,true);
    $tablero = new Tablero($resp_tablero['id'],$resp_tablero['name']);
    foreach($resp_tablero['lists'] as $lista){
      $lista = new Lista($lista['id'],$lista['name']);
      ClienteTrello::obtenerCartas($token_usuario,$app_key,$idtablero,$lista);
      $tablero->anadirLista($lista);
    }
    return $tablero;
  }

  private static function obtenerCartas($token_usuario,$app_key,$idtablero,$lista){
    $client = new Client();
    $resultado = $client->get(ClienteTrello::URL_OBTENER_CARTAS.$lista->id.ClienteTrello::URL_OBTENER_CARTAS2.'&key='.$app_key.'&token='.$token_usuario);
    if ($resultado->getStatusCode() == 200){
      $resp_cartas = json_decode($resultado->getBody(),true);
      foreach ($resp_cartas['cards'] as $cartaJ){
        $carta = new Tarjeta($cartaJ['id'],$cartaJ['name']);
        $lista->anadirTarjeta($carta);
      }
    }
  }

  public static function actualizarTarjeta($token_usuario,$app_key,$idtarjeta,$nombre){
    $client = new Client();
    $resultado = $client->put(ClienteTrello::URL_EDITAR_CARTA.$idtarjeta.ClienteTrello::URL_EDITAR_CARTA_2.$nombre.'&key='.$app_key.'&token='.$token_usuario);
    if ($resultado->getStatusCode() == 200){
      $tarjeta_editada = json_decode($resultado->getBody(),200);
      return new Tarjeta($tarjeta_editada['id'],$tarjeta_editada['name']);
    }
    return null;
  }

  public static function crearTarjeta($token_usuario,$app_key,$id_lista,$nombre){
    $client = new Client();
    $resultado = $client->post(ClienteTrello::URL_CREAR_CARTA.$id_lista.ClienteTrello::URL_CREAR_CARTA_2.$nombre.'&key='.$app_key.'&token='.$token_usuario);
    if ($resultado->getStatusCode() == 200){
      $tarjeta_editada = json_decode($resultado->getBody(),200);
      return new Tarjeta($tarjeta_editada['id'],$tarjeta_editada['name']);
    }
    return null;
  }

  public static function eliminarTarjeta($token_usuario,$app_key,$id){
    $cliente = new Client();
    $resultado = $cliente->delete(ClienteTrello::URL_ELIMINAR_CARTA.$id.'?key='.$app_key.'&token='.$token_usuario);
    if ($resultado->getStatusCode() == 200){
      return true;
    }
    return false;
  }

}
